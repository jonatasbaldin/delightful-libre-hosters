# Delightful Libre Hosters
[![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A list of people and organizations who offer open source services to the public, free or paid.

## Free as in Speech
- [Disroot](#disroot)
- [HelioHost](#heliohost)
- [hen.ee](#hen-ee)
- [Infra4future](#infra4future)
- [LibreOps](#libreops)
- [NixNet](#nixnet)
- [Nomagic](#nomagic)
- [OpenDesktop](#opendesktop)
- [Riseup](#riseup)
- [Slowb](#slowb)
- [Snopyta](#snopyta)
- [sp-codes](#sp-codes)
- [Systemli](#systemli)
- [tchncs](#tchncs)
- [Trom.tf](#trom-tf)
- [Uncloud](#uncloud)

## Paid
- [Autonomic](#autonomic)
- [Cloud68](#cloud68)
- [CommonsCloud](#commonscloud)
- [Indie Hosters](#indie-hosters)
- [Fedi.Monster](#fedi-monster)
- [MastoHost](#mastohost)
- [Mayfirst](#may-first)
- [meet.coop](#meet-coop)
- [Nomagic](#nomagic)
- [Ossrox](#ossrox)
- [PikaPods](#pikapods)
- [Rad Web Hosting](#rad-web-hosting)
- [Snikket](#snikket)
- [Spacebar Federation](#spacebear-federation)
- [The Good Cloud](#the-good-cloud)
- [Togethr](#togethr)
- [WebApe](#webape)
- [Webarchitects Cooperative](#webarchitects-cooperative)
- [Weingaertner IT Services](#weingaertner-it-services)

## Groups
- [Groups of Libre Hosters](#groups-of-libre-hosters)

# Free as in Speech

## Disroot
> Disroot is a platform providing online services based on principles of freedom, privacy, federation and decentralization.

Pricing: free.

Services: Email, Nextcloud, Discourse, XMPP, Etherpad, EtherCalc, PrivateBin, Lufi, Searx, Framadate, Taiga, Jitsi Meet, Forgejo, Mumble, Cryptpad. 

[Go to page](https://disroot.org/en).

## HelioHost
> Create your own website quickly and easily with the best free hosting, HelioHost.

Pricing: free, requires registration.

Services: Web hosting, email.

[Go to page](https://heliohost.org).

## hen.ee
> One of the most vital things I've done during the pandemic and the privatization of my digital life is start self-hosting alternatives to popular services (such as Google). It's a great choice I made, and I encourage everyone to do something as well, however most of us don't have the time or courage needed to start administrating Linux servers. That is why from 2021, I'll start providing some of my instances to the public for free. I don't like long and complicated ToS or Privacy Policies, so just make sure to be respectful towards the administrator and servers and I'll not track or sell your data.

Pricing: free.

Services: Searx, Teddit, Nitter, Jitsi.

[Go to page](https://hen.ee/instances/).

## infra4future 
> Infra4future.de was based on the need of various activist groups in the Munich area to work better together, if possible without joining more and more confusing individual chat groups or working with other usual (proprietary and commercial) platforms such as Slack or similar. This server was built by hacc as a free alternative to such services, to have a platform that is by and for the movement. 

Pricing: free, requires registration.

Services: Nextcloud, Mattermost, Discourse, Mumble, LimeSurvey, GitLab.

[Go to page](https://infra4future.de/).

## LibreOps
> We believe that building, owning and controlling technology is important. Hackers and Open Source communities have an opportunity and a responsibility to offer an alternative, to offer services and tools that people can trust. Beyond corporate control, that comes with tracking and data farming. Decentralized services and tools that respect users privacy by default.

Pricing: free.

Services: LibreDNS, radicalDNS, Etherpad, Jabber, Konekti, Diskuti, Mastodon, Jitsi Meet, Mumble, Syncthing Relay, Tor Relays.

[Go to page](https://libreops.cc/).

## NixNet
> Away from prying eyes... We host a variety of services that are available for anyone to use free of charge. 

Pricing: free.

Services: Jabber/XMPP, BigBlueButton, Pleroma, Misskey, Mobilizon, Forgejo, HedgeDoc, Mumble, Searx, Wallabag, Etherpad, PrivateBin, Linx, Tiny Tiny RSS, xBrowserSync, Nitter, Scribe, RSS-Bridge, Framadate.

[Go to page](https://nixnet.services/).

## Nomagic
> Nomagic aims to be a trusted partner of your digital life.  To achieve this, we provide a large catalogue of services hosted on our servers. We do not engage in data monetization whatsoever, and believe that people's data and online activity should not be traded in any way. 

Pricing: Offer some free services (besides their paid tier).

Services: Mobilizon, Framadate, Turtl, PrivateBin, EtherCalc, RSS-Bridge. 

[Go to page](https://nomagic.uk/).

## OpenDesktop
> Opendesktop.org is a libre platform providing free cloud storage, online office editing, contacts & calender tools, personal chat and messaging, as well as project development and product publishing to anyone who values freedom and openness.

Pricing: free, account required.

Services: Products (ocs-webserver), GitLab, Nextcloud, Discourse, Matrix, Discourse, OpenStreetMaps

[Go to page](https://www.opendesktop.org/).

## Riseup

> Riseup provides online communication tools for people and groups working on liberatory social change. We are a project to create democratic alternatives and practice self-determination by controlling our own secure means of communications. Funded by donations.

Pricing: free, account required for red and black services.

Red services: Email (IMAP & webmail), VPN  
Black services: Email lists  
Green services: Private wikis, Group collaboration, Pad, Pastebin and Imagebin, Security guides

[Go to page](https://riseup.net/).

## Slowb
> Open & Libre. Private & Secure. We can have both, without compromising on the others. SysOps should always be automated.

Pricing: free.

Services: Gitea, Woodpecker, SearXNG, Mumble, Container registry.

[Go to page](https://slowb.ro/).

## Snopyta
> Snopyta runs online services based on freedom, privacy and decentralization.

Pricing: free.

Services: Searx, PrivateBin, HeadgeDoc, CyberChef, Framadate, Jitsi Meet, Invidious, Nitter, XMPP, Mastodon, Tiny Tiny RSS.

[Go to page](https://snopyta.org/).

## sp-codes
> sp-codes provides various open source services hosted in Germany for free use.

Pricing: free, no account required.

Services: Matrix, Jitsi, Mastodon, PeerTube, Forgejo.

[Go to page](https://sp-codes.de/).

## Systemli
> Systemli is a left-wing network and technics-collective that was founded in 2003. Our aim is to provide safe and trustworthy communication services. The project is primarily aimed at left-wing political activists and people who have a particular need to protect their data. Our answer to this need is based on the user’s trust in us. We protect their data by encrypting all our servers and connections (except connections to a few remaining mailservers that don’t support TLS) and by avoiding to retain unnecessary connection data. Therefore, in case of an unauthorized access, the data is protected.

Pricing: free.

Services: Nextcloud, Email, Jabber, Jitsi Meet, Matrix, PrivateBin, Schleuder, Ticker, Web Hosting.

[Go to page](https://www.systemli.org/).

## tchncs
> Explore some of the finest open-source software out there! tchncs is run by an individual, ad-free and funded by voluntary donations of its users. Due to good ranking in serverlists, some services like Mastodon or Matrix became quite popular around 2017. 

Pricing: free.

Services: Mastodon, Matrix, Pixelfed, PeerTube, The World of Illuna, Veloren, Vaultwarden, Cryptpad, hasteBin, GitLab, Mumble, Jabber, Plume.

[Go to page](https://tchncs.de/).

## TROM.tf
> PROVIDING TRADE-FREE ONLINE SERVICES FOR EVERYONE, because our trade-based society is ruining everything, and we want to fix that!

Pricing: free.

Services: Friendica, Nextcloud, PeerTube, FacilMap, Matrix, CryptPad, Searx, Jitsi, Lufi, Gitea, RSS Bridge, Wallabag, Diagrams.net, OpenSondage, Etherpad, WemaWema, LibreQR, hasteBin, Lstu, Nitter, Teddit, LibreSpeed, Invidious.

[Go to page](https://trom.tf/).

## Uncloud
> uncloud.do is a collection of small, open source and privacy focused software services.

Pricing: free.

Services: One Time Secret, Libre Translate, FreshRSS, Nitter, Nextcloud, Matrix, Cryptpad, Invidious.

[Go to page](https://uncloud.do).

# Paid

## Autonomic 
> Autonomic is a co-operative that is owned and run by its workers. We build technologies and infrastructure to empower users to make a positive impact on the world. All of our services reflect our commitment to our core values: sustainability, transparency, privacy.

Pricing: consultation required.

Services: Wordpress, Rocket.Chat, Netxcloud, Discourse.

[Go to page](https://autonomic.zone).

## Cloud68
> At Cloud68.co we provide solopreneurs, SMEs and organisations with reliable, safe and ready2use digital infrastructure. We do this by using only open source solutions following our strong belief that YOU should have more control and privacy over your tech infrastructure. Bonus: human support!

Pricing: different per service, some services require you to get a quote or schedule a call with them.

Services: BigBlueButton, Bitwarden, Discourse, DokuWiki, Editoria, Funkwhale, Ghost, GitLab, Invoice Ninja, Kotahi, Wordpress, Matomo, MediaWiki, Metabase, Mattermost, PeerTube, WeKan, Nextcloud, Zammad, WriteFreely.

[Go to page](https://cloud68.co/).

## CommonsCloud
> CommonsCloud.coop is made up of people and organisations that pool digital cloud resources in a technological and cooperative project with free software; people and organisations responsible for the implementation, maintenance, management and support of the service.

Pricing: different per service and per tier. A 'takeTheLeap' service can be bought to help in a migration.

Services: BigBlueButton (via Meet.coop), Collabora Online, Discourse, Dolibarr, LimeSurvey, NextCloud, Zimbra Collaboration.

[Go to page](https://www.commonscloud.coop/en/)

## Indie Hosters
> We are a collective of people from diverse backgrounds, endowed with specific skills/networks, who develop a transversal view sensitive to human, ecological, technical, economic, educational, artistic, legal and political issues and driven by the desire to include IndieHosters in the future of commons digital (translated from French).

Pricing: starts at 8EUR per month.

Services: Nextcloud, OnlyOffice, Rocket.Chat, Jitsi Meet.

[Go to page](https://indiehosters.net).

## Fedi.Monster
> First known as 'MaaStodon', it has been operating since 2017, for a few internet communities to make their Mastodon hosting more reliable and affordable, and let others run their own instance for a reasonable price instead of all the separate costs, time spent, and required knowledge. We even hosted mastodon.social before it was migrated to its own dedicated infrastructure, with our help. 

Pricing: From 5€ and 9€.

Services: Mastodon, Pixelfed.

[Go to page](https://fedi.monster/).

## MastoHost
> Fully managed Mastodon hosting.

Pricing: Starting from 6EUR/month.

Services: Mastodon.

[Go to page](https://masto.host).

## May First
> May First Movement Technology is a non-profit membership organization that engages in building movements by advancing the strategic use and collective control of technology for local struggles, global transformation, and emancipation without borders.

Pricing: $25/user per year or $50 for an organization. See more information [here](https://mayfirst.coop/en/member-benefits/).

Services: Website, email, email lists, Jitsi Meet, Nextcloud, XMPP and more.

[Go to page](https://mayfirst.coop/en/).

## Meet.Coop
> The Online Meeting Cooperative is privacy-respecting, commons-oriented online meeting service. It is supported by an international coalition who share the development of collaborative digital infrastructure, embedded in a community of mutual support. meet.coop is powered by open source tools, renewable energy and solidarity. The service is based on BigBlueButton, with servers in North America and Europe.

Pricing: From 9£ to 90£.

Services: BigBlueButton.

[Go to page](https://www.meet.coop/).

## Nomagic
> Nomagic aims to be a trusted partner of your digital life.  To achieve this, we provide a large catalogue of services hosted on our servers. We do not engage in data monetization whatsoever, and believe that people's data and online activity should not be traded in any way. 

Pricing: requires membership of 3 months minimum, they have a Standard and Insecure Situation membership. Offer some free services.

Services: Email (bundle of open source services), Seafile, Lufi, Vaultwarden, Matrix Synapse, Tiny Tiny RSS, Etherpad, HedgeDoc, OnlyOffice, Kanboard, Ejabberd, DokuWiki, WordPress, Static web hosting, TiddlyWiki, Pleroma, GitLab, PeerTube, Funkwhale, Wallabag, Lutim, LSTU, Jitsi Meet, Lime Survey, Sympa, Mobilizon, Framadate, Turtl, PrivateBin, EtherCalc, RSS-Bridge. 

[Go to page](https://nomagic.uk/).

## Ossrox
> Ossrox is a company from Germany dedicated to hosting open-source services. Not only has the company name been derived from the motto "Open-Source Software Rocks", but also their corporate culture. The three most important principles of the company are security, privacy and sustainability - thanks to open-source software.

Pricing: paid.

Services: Matrix, Nextcloud, Jitsi, Mastodon, PeerTube, Owncast and many more.

[Go to page](https://ossrox.org/).

## PikaPods
> Run the finest Open Source web apps from just $1/month. Start free with $5 welcome credit. Fully managed, no servers to administer. No tracking, no ads, no snooping.

Pricing: paid (hourly/monthly), prices according resource consumption.

Services: Like, a lot? All the major open source tools.

[Go to page](https://pikapods.com/)

## Rad-Web-Hosting
> Run any Open Source web apps on 100% SSD-powered KVM VPS from just $2.49/month. Full-route access makes it a robust solution for production and development use-cases.

Pricing: paid (monthly), prices according resource consumption.

Services: All the major open source tools.

[Go to page](https://radwebhosting.com/)

## Snikket
> We can also run your Snikket service for you. It’s a great option if you’re just looking for a more privacy-friendly alternative to mainstream messengers such as Facebook, WhatsApp or Telegram, without the learning curve required to run your own server. You can always migrate your data at any time.

Pricing: Free while in beta, probably 2EUR/month per user later.

Services: Snikket (XMPP).

[Go to page](https://snikket.org/hosting/).

## Spacebear federation
> Join the fediverse. Create your own social network.

Pricing: varies according to the service, but starts on about $16~$20 per month.

Services: Pixelfed, Mastodon, PeerTube, Hometown, Pleroma.

[Go to page](https://federation.spacebear.ee/).

## The Good Cloud
>  Our goal is to provide a secure cloud solution, a platform where you can store all your files with peace of mind. You don’t have to worry about us viewing, analyzing or selling your data. We are proud partners of Nextcloud GmbH.

Pricing: free account with 2GB, charges monthly/yearly for more storage.

Services: Nextcloud, CollaboraOffice, OnlyOffice.

[Go to page](https://thegood.cloud).

## Togethr
> Get your very own Mastodon-compatible fediverse instance. Perfect for individuals, families, or small groups.

Pricing: 30 day free trial, then $7/month.

Services: Pleroma and Soapbox.

[Go to page](https://togethr.party/).

## WebApe
> WebDesign + Hosting + Management. The creator of Trom.tf.

Pricing: varies per service.

Services: Wordpress, PeerTube, Mastodon, Nextcloud. 

[Go to page](https://webape.site/).

## Webarchitects Cooperative
> Webarchitects is a multi-stakeholder co-operative based in Sheffield, UK, we have been established as a co-op for over ten years and provide managed, ethical, green, open source, cloud hosting services for individuals and organisations.

Pricing: varies.

Services: Shared website hosting, VPS and email services.

[Go to page](https://www.webarchitects.coop/).

## Weingaertner IT Services
> Weingaertner IT Services provide managed, ethical, green, open source, cloud hosting services for individuals and organisations.

Pricing: varies.

Services: Shared Website Hosting, Mastodon, WordPress, PeerTube, Funkwhale, Lerntools, Castopod, BookWyrm, Mobilizon, FireFish, Managed Server,
E-Mail, Matrix/Synapse, Lemmy, Pixelfed, akkoma, Hubzilla, Iceshrimp, Sharkey, write freely, Friendica, Wazuh

[Go to page](https://weingaertner-it.de/).

# Groups of Libre Hosters
- [Chatons](https://www.chatons.org): CHATONS – kittens in french – is the Collective of Hosters Alternative, Transparent, Open, Neutral and Solidarity. This collective aims to bring together structures offering free, ethical and decentralised online services in order to allow users to quickly find alternatives that respect their data and privacy to the services by GAFAM (Google, Apple, Facebook, Amazon, Microsoft).
- [librehosters](https://libreho.st/): librehosters is a network of cooperation and solidarity that uses free software to encourage decentralisation through federation and distributed platforms. Our values connect transparency, fairness and privacy with a culture of data portability and public contributions to the commons.

## Maintainers
If you have questions or feedback regarding this list, then please create an [Issue in our tracker](https://codeberg.org/jonatasbaldin/delightful-sustainable-vps/issues), and optionally @mention one or more of our maintainers:
- [@jonatasbaldin](https://codeberg.org/jonatasbaldin)

## Contributors
With delight we present you some of our [delightful contributors](delightful-contributors.md) (please add yourself if you are missing).

## License
[CC 4.0 International](LICENSE).
