# Delightful contributors
These fine people brought us delight by adding their gems of freedom to the delight project.

> **Note**: This page is maintained by you, contributors. Please create a pull request or issue in our tracker if you contributed list entries and want to be included. [More info here](https://codeberg.org/teaserbot-labs/delightful/src/branch/master/delight-us.md#attribution-of-contributors).

- [Jonatas Baldin](https://jojo.garden)
- [Arnold Schrijver](https://community.humanetech.com/u/aschrijver/summary)
  (codeberg: [@circlebuilder](https://codeberg.org/circlebuilder),
  fediverse: [@humanetech@mastodon.social](https://mastodon.social/@humanetech))
- [Peter H.](https://ravenation.club/@peterhil)
- [Caesar Schinas](https://caesarschinas.com/) (codeberg: [@caesar](https://codeberg.org/caesar), fediverse: [@caesar@indieweb.social](https://indieweb.social/@caesar))
